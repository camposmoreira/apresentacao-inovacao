### Ideia Geral por tras de MLops
  -> Extração de dados
    - Extração
    - Consumo 
    - processamento

  -> Treino
    - treino
    - Tuning do modelo
    - selecao
  
  -> Deploy da aplicação
    - App em producao
    - Monitoramento
  

### Kubeflow até agora
 - Kubeflow é um projeto para deploy ML no kubernetes

 - Componentes:
   - Pipelines
   - Experimentos
   - Runs

 - Pipelines:
   - Etapas que o ML executa
   - Cada parte da pipeline pode rodar em diferentes conteiners. Ex: Treinamento no pyhton 3.5.2 e parametrização no 3.10

 - Experimentos:
   - Espaço aonde é associado todas as pipelines e suas respectivas runs

 - Runs:
   - Quando a pipeline é executada e seus parametros de entrada
 
 - Integração com o CI/CD

### Investigações/Problemas/Proximos passos
- Problemas de compabilidade:
  - https://awslabs.github.io/kubeflow-manifests/docs/about/eks-compatibility/
  - https://github.com/kubeflow/kubeflow/issues/6700
 - Autentificação:
   - Ta sendo logado por login e senha. Verificar se há outra forma
 - CI/CD
   - Ativação ta sendo feita por script python. Verificar se da para utilizar kubernetes diretamente
   - Adicionar mais de uma pipeline num repositorio
   - Verificar outras formas alem dos jobs do gitlab
   - O pacote roda autonomo, ou precisa de um ambiente specifico para poder rodar
 - Artefatos/Dados
   - Como utiliza o volume/s3
   - Como funciona as questões de segurança dos dados no ponto de vista tecnico
 - Deploy/Produção
   - Como eh feito o retreino dos modelos em producao
   - Como seriam diferente ambientes de sandbox/produção
 - Instalação
   - A instalação foi feita num ambiente vanilla. Teria problemas ao interagir com outros serviços?
 - A POC está muito hardcoded
 - Monitoramento
 - Componentes:
   - Volumes
   - Notebooks
   - Tensorboards
   - Modelserver
     - https://www.kubeflow.org/docs/external-add-ons/kserve/webapp/
---

# CML

### cml-base-case
https://gitlab.com/camposmoreira/cml-base-case
https://cml.dev/
 - CML => Continuous Machine Learning
 - Para rodar o exemplo de teste, fixar as versões do requirments e setar o repo_token
 - https://gitlab.com/camposmoreira/cml-base-case/-/commit/3e9fef16a3ae28d4dbbcd0a8e940990409cc6830
 - Adiciona uma imagem de treinamento no MR

[//]: # (XXXX)

### cml-dvc-case
 - DVC => Data Version Control
 - dvc remote add -d storage s3://dvc-example-moreira/dvcstore
 - Configurar credenciais s3
 - https://gitlab.com/camposmoreira/cml-dvc-case/-/merge_requests/2#note_1225431307

### Mlem
https://mlem.ai/doc/get-started
 - Tem problemas com o poetry
 - Empacotador de modelos ML's
 - Parecido com funções lambdas

### GTO
 - Git Tag Ops.
 - Taggeador de tags para modelos

### Example get Start
 - https://studio.iterative.ai/user/camposmoreira/pricing
 - https://gitlab.com/camposmoreira/example-get-start-iterative
 - https://studio.iterative.ai/user/camposmoreira/projects/example-get-start-iterative-mo4chbfclz?commits=5900214%2Cprimary%3B5900212%2Cpurple

 - Interessantes as comparacoes
 - Entender mais sobre os modelos



---

### Gitlab

# MLops/Gitlab
https://www.youtube.com/watch?v=SFhJ7kRJADA
https://www.youtube.com/watch?time_continue=24&v=7mUgGFgab4E&feature=emb_title
https://gitlab.com/gitlab-org/modelops/applied-ml/review-recommender/suggested-reviewer-client

### fashion-mnist-imda
https://gitlab.com/camposmoreira/fashion-mnist-imda/-/jobs/3542790534

 - Demo do time de mlops do gitlab, para mostrar como funcionaria com kubernetes.
 - Usa a CI/CD do gitlab para validar/treinar/deployar
 - Divido nestes estagios:
   - validate
   - build
   - train
   - deploy
   - test
   - cleanup
 - Não é uma boa demo. Teve alguns problemas para rodar e não deploya se ter que fazer alguns ajustes

### covmod
https://gitlab.com/camposmoreira/covmod
 - Em teoria demo para mostrar os dados do time de mlops do gitlab
 - Não tem nada

### Car crashes
https://gitlab.com/camposmoreira/car_crashes

 - Não muito utilpara testes. Não roda direito. Nem os comandos do readme funcionam bem
 - Utiliza o Growing data


### Tutorial com pipelines
https://gitlab.com/camposmoreira/mlops-gitlab-heroku
https://heartbeat.comet.ml/mlops-pipeline-with-gitlab-in-minutes-57173ba8d692
 - Tutorial simples para como usar o CI/CD do gitlab para fazer deploys
 - Usa heroku. Que deixou de ser gratuito 3 semanas atrás


# Hyper Model
https://github.com/GrowingData/hyper-model

- Não atualizado alguns anos
- Alguns problemas na hora de rodar
- A teoria por tras dele eh fazer um pipeline de deploy de modelos em produção
- Dois exemplos:
  - Car crashed
  - tragic titnatic





TESTE MOREIRA
# Livros
### Machine Learning Engineering by Andriy Burkov
 -> Reler com calma a partir do capitulo 4, 8, 9
 -> A ideia mais geral parece ser a construção de algoritmos de machine learning
 

### Introducing MLOps: How to Scale Machine Learning in the Enterprise

### The Hundred-Page Machine Learning Book-Andriy Burkov (2019)