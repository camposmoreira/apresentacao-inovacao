### Projeto para apresentar o kubeflow
Subir:

Conectar no projeto:

Em um terminal:

ssh url_da_maquina  -i chave_de_acesso.pem 

cd kubeflow-manifests/
make port-forward


Em outro terminal:

ssh -i chave_de_acesso.pem  -L 8080:localhost:8080 -N url_da_maquina -o ExitOnForwardFailure=yes


Em outro terminal
ngrok http 8080 --hostname kubeflow-mindsight.ngrok.io

