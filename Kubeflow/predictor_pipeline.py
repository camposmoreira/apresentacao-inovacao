#!/usr/bin/env python3

import kfp
from kfp import dsl

def run_python_3_5_2():
    return dsl.ContainerOp(
        name='python rodando o 3.5.2',
        image='python:3.5.2-alpine',
        command=['sh', '-c'],
        arguments=['python --version | tee /tmp/output'],
        file_outputs={
            'data': '/tmp/output',
        }
    )

def flip_coin_op_3_6():
    """Flip a coin and output heads or tails randomly."""
    return dsl.ContainerOp(
        name='Flip coin',
        image='python:alpine3.6',
        command=['sh', '-c'],
        arguments=['python -c "import random; result = \'heads\' if random.randint(0,1) == 0 '
                  'else \'tails\'; print(result)" | tee /tmp/output'],
        file_outputs={'output': '/tmp/output'}
    )

def flip_coin_op_3_7():
    """Flip a coin and output heads or tails randomly."""
    return dsl.ContainerOp(
        name='Flip coin',
        image='python:alpine3.7',
        command=['sh', '-c'],
        arguments=['python -c "import random; result = \'heads\' if random.randint(0,1) == 0 '
                  'else \'tails\'; print(result)" | tee /tmp/output'],
        file_outputs={'output': '/tmp/output'}
    )

def echo_op(text):
    return dsl.ContainerOp(
        name='echo',
        image='library/bash:4.4.23',
        command=['sh', '-c'],
        arguments=['echo "$0"', text]
    )

@dsl.pipeline(
    name='sequential-pipeline',
    description='A pipeline'
)
def sequential_pipeline():
    python_version = flip_coin_op_3_6()
    python_version = flip_coin_op_3_7()
    python_version = run_python_3_5_2()
    echo_task = echo_op(python_version)

if __name__ == '__main__':
    kfp.compiler.Compiler().compile(sequential_pipeline, __file__ + '.yaml')
