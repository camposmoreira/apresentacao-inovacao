
def list_clients(client, namespace):
    print(client.list_experiments(namespace=namespace))

def upload_and_run(client,pipeline_package_path, pipeline_name, pipeline_version, experiment_id):
    pipeline_created = client.upload_pipeline_version(pipeline_package_path=pipeline_package_path,pipeline_version_name=pipeline_version,pipeline_name=pipeline_name)

    print(pipeline_created)
    print("----")
    print(dir(pipeline_created))
    version_id = pipeline_created.id
    pipeline_id = pipeline_created.resource_references[0].key.id
    run_ = client.run_pipeline(experiment_id=experiment_id, job_name="job" + pipeline_id ,pipeline_id=pipeline_id, version_id=version_id)
    print(run_)

    # print(client.create_run_from_pipeline_package('predictor_pipeline.yaml', arguments={}, run_name="login_run", namespace=KUBEFLOW_NAMESPACE))

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Just an example",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--pipeline-version", help="Pipeline version")
    parser.add_argument("--pipeline-package-path", help="path")
    args = parser.parse_args()
    config = vars(args)

    KUBEFLOW_NAMESPACE = "kubeflow-user-example-com"

    from get_client import get_client

    client = get_client()
    id ='becc888d-5625-46cb-ac33-1aa2ba2577ff'
    experiment_id = "852953fa-508d-446a-92ce-4de14f7980c4"
    upload_and_run(client=client,pipeline_package_path=config["pipeline_package_path"], pipeline_name="gitlab-created-action", pipeline_version=config["pipeline_version"], experiment_id=experiment_id)
    # foo(name=)
