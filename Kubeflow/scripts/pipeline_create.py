
def create_pipeline(client, name, description):
    print(client.upload_pipeline(pipeline_package_path='../gitlab_.yaml', pipeline_name=name,description=description))

if __name__ == "__main__":
    from get_client import get_client

    client = get_client()
    create_pipeline(client=client, name="gitlab-created-action", description="Created at gitlab action")
